# For each cylinder in the scan, find its ray and depth.
# 03_c_find_cylinders
# Claus Brenner, 09 NOV 2012
from pylab import *
from lego_robot import *
import numpy as np


# Find the derivative in scan data, ignoring invalid measurements.
def compute_derivative(scan, min_dist):
    jumps = [0]
    for i in range(1, len(scan) - 1):
        l = scan[i - 1]
        r = scan[i + 1]
        if l > min_dist and r > min_dist:
            derivative = (r - l) / 2.0
            jumps.append(derivative)
        else:
            jumps.append(0)
    jumps.append(0)
    return jumps


# For each area between a left falling edge and a right rising edge,
# determine the average ray number and the average depth.
def find_cylinders(scan, scan_derivative, jump, min_dist):
    cylinder_list = []

    sum_ray, sum_depth, rays = 0.0, 0.0, 0
    walls_indexes = []

    for i in range(len(scan_derivative)):
        # --->>> Insert your cylinder code here.
        # Whenever you find a cylinder, add a tuple
        # (average_ray, average_depth) to the cylinder_list.
        if scan_derivative[i] > 100 and ("r", i - 1) not in walls_indexes:
            walls_indexes.append(("r", i))
        if scan_derivative[i] < -100 and ("l", i - 1) not in walls_indexes:
            walls_indexes.append(("l", i))
    print(f"wall indexes in {walls_indexes}")

    index = 0
    next_index = index + 1
    while next_index < len(walls_indexes):
        if walls_indexes[next_index][0] != walls_indexes[index][0]:
            i = walls_indexes[index][1]
            i_plus = walls_indexes[next_index][1]
            rays = i_plus - i
            avg_ray = rays / 2 + i
            relevant_scans = [rel for rel in scan[i:i_plus] if rel > min_dist]
            avg_depth = np.average(relevant_scans)
            cylinder_list.append((avg_ray, avg_depth))
            print(f"cylinder in {(avg_ray, avg_depth)}")
            index = next_index + 1
            next_index = index + 1
        else:
            next_index += 1

    return cylinder_list


if __name__ == '__main__':
    minimum_valid_distance = 20.0
    depth_jump = 100.0

    # Read the logfile which contains all scans.
    logfile = LegoLogfile()
    logfile.read("robot4_scan.txt")

    # Pick one scan.
    scan = logfile.scan_data[8]

    # Find cylinders.
    der = compute_derivative(scan, minimum_valid_distance)
    cylinders = find_cylinders(scan, der, depth_jump,
                               minimum_valid_distance)

    # Plot results.
    plot(scan)
    plot(der)
    scatter([c[0] for c in cylinders], [c[1] for c in cylinders],
            c='r', s=200)
    show()
