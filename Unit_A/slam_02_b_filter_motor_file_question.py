# Implement the second move model for the Lego robot.
# The difference to the first implementation is:
# - added a scanner displacement
# - added a different start pose (measured in the real world)
# - result is now output to a file, as "F" ("filtered") records.
#
# 02_b_filter_motor_file
# Claus Brenner, 09 NOV 2012
from math import sin, cos, pi
from lego_robot import *


# This function takes the old (x, y, heading) pose and the motor ticks
# (ticks_left, ticks_right) and returns the new (x, y, heading).
def filter_step(old_pose, motor_ticks, ticks_to_mm, robot_width,
                scanner_displacement):
    left = motor_ticks[0] * ticks_to_mm
    right = motor_ticks[1] * ticks_to_mm
    theta = old_pose[2]
    old_x = old_pose[0]
    old_y = old_pose[1]
    old_x = old_x - scanner_displacement * cos(theta)
    old_y = old_y - scanner_displacement * sin(theta)
    # Find out if there is a turn at all.
    if motor_ticks[0] == motor_ticks[1]:
        # No turn. Just drive straight.

        # --->>> Use your previous implementation.
        # Think about if you need to modify your old code due to the
        # scanner displacement?
        # No turn. Just drive straight.
        x = old_x + left * cos(theta) + scanner_displacement * cos(theta)
        y = old_y + left * sin(theta) + scanner_displacement * sin(theta)
        # --->>> Implement your code to compute x, y, theta here.
        return (x, y, theta)

    else:
        # Turn. Compute alpha, R, etc.

        # --->>> Modify your previous implementation.
        # First modify the the old pose to get the center (because the
        #   old pose is the LiDAR's pose, not the robot's center pose).
        # Second, execute your old code, which implements the motion model
        #   for the center of the robot.
        # Third, modify the result to get back the LiDAR pose from
        #   your computed center. This is the value you have to return.

        alpha = (right - left) / robot_width
        R = left / alpha
        distance_to_center = R + robot_width / 2
        center_x, center_y = calc_center(distance_to_center, (old_x, old_y, theta))

        theta = (old_pose[2] + alpha) % (2 * pi)
        x = center_x + distance_to_center * sin(theta)
        y = center_y + distance_to_center * cos(theta) * (-1)

        x = x + scanner_displacement * cos(theta)
        y = y + scanner_displacement * sin(theta)

        return (x, y, theta)


def calc_center(distance_to_center, old_pose):
    center_x = old_pose[0] - distance_to_center * sin(old_pose[2])
    center_y = old_pose[1] - distance_to_center * cos(old_pose[2]) * (-1)
    return center_x, center_y


if __name__ == '__main__':
    # Empirically derived distance between scanner and assumed
    # center of robot.
    scanner_displacement = 30.0

    # Empirically derived conversion from ticks to mm.
    ticks_to_mm = 0.349

    # Measured width of the robot (wheel gauge), in mm.
    robot_width = 150.0

    # Measured start position.
    pose = (1850.0, 1897.0, 213.0 / 180.0 * pi)

    # Read data.
    logfile = LegoLogfile()
    logfile.read("robot4_motors.txt")

    # Loop over all motor tick records generate filtered position list.
    filtered = []
    for ticks in logfile.motor_ticks:
        pose = filter_step(pose, ticks, ticks_to_mm, robot_width,
                           scanner_displacement)
        filtered.append(pose)

    # Write all filtered positions to file.
    f = open("poses_from_ticks.txt", "w")
    for pose in filtered:
        print("F %f %f %f" % pose, file=f)
    f.close()
