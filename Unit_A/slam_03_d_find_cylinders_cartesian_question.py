# For each cylinder in the scan, find its cartesian coordinates,
# in the scanner's coordinate system.
# Write the result to a file which contains all cylinders, for all scans.
# 03_d_find_cylinders_cartesian
# Claus Brenner, 09 NOV 2012
from lego_robot import *
from math import sin, cos
import numpy as np


# Find the derivative in scan data, ignoring invalid measurements.
def compute_derivative(scan, min_dist):
    jumps = [0]
    for i in range(1, len(scan) - 1):
        l = scan[i - 1]
        r = scan[i + 1]
        if l > min_dist and r > min_dist:
            derivative = (r - l) / 2.0
            jumps.append(derivative)
        else:
            jumps.append(0)
    jumps.append(0)
    return jumps


# For each area between a left falling edge and a right rising edge,
# determine the average ray number and the average depth.
def find_cylinders(scan, scan_derivative, jump, min_dist):
    cylinder_list = []

    sum_ray, sum_depth, rays = 0.0, 0.0, 0
    walls_indexes = []

    for i in range(len(scan_derivative)):
        # --->>> Insert your cylinder code here.
        # Whenever you find a cylinder, add a tuple
        # (average_ray, average_depth) to the cylinder_list.
        if scan_derivative[i] > 100 and ("r", i - 1) not in walls_indexes:
            walls_indexes.append(("r", i))
        if scan_derivative[i] < -100 and ("l", i - 1) not in walls_indexes:
            walls_indexes.append(("l", i))
    print(f"wall indexes in {walls_indexes}")

    index = 0
    next_index = index + 1
    while next_index < len(walls_indexes):
        if walls_indexes[next_index][0] != walls_indexes[index][0]:
            i = walls_indexes[index][1]
            i_plus = walls_indexes[next_index][1]
            rays = i_plus - i
            avg_ray = rays / 2 + i
            relevant_scans = [rel for rel in scan[i:i_plus] if rel > min_dist]
            avg_depth = np.average(relevant_scans)
            cylinder_list.append((avg_ray, avg_depth))
            print(f"cylinder in {(avg_ray, avg_depth)}")
            index = next_index + 1
            next_index = index + 1
        else:
            next_index += 1

    return cylinder_list


def compute_cartesian_coordinates(cylinders, cylinder_offset):
    result = []
    for c in cylinders:
        # --->>> Insert here the conversion from polar to Cartesian coordinates.
        # c is a tuple (beam_index, range).
        # For converting the beam index to an angle, use
        # LegoLogfile.beam_index_to_angle(beam_index)
        theta = LegoLogfile.beam_index_to_angle(c[0])
        r = c[1] +cylinder_offset
        x = r * cos(theta)
        y = r * sin(theta)
        result.append((x, y))  # Replace this by your (x,y)
    return result


if __name__ == '__main__':

    minimum_valid_distance = 20.0
    depth_jump = 100.0
    cylinder_offset = 90.0

    # Read the logfile which contains all scans.
    logfile = LegoLogfile()
    logfile.read("robot4_scan.txt")

    # Write a result file containing all cylinder records.
    # Format is: D C x[in mm] y[in mm] ...
    # With zero or more points.
    # Note "D C" is also written for otherwise empty lines (no
    # cylinders in scan)
    out_file = open("cylinders.txt", "w")
    for scan in logfile.scan_data:
        # Find cylinders.
        der = compute_derivative(scan, minimum_valid_distance)
        cylinders = find_cylinders(scan, der, depth_jump,
                                   minimum_valid_distance)
        cartesian_cylinders = compute_cartesian_coordinates(cylinders,
                                                            cylinder_offset)
        # Write to file.
        print("D C", end=' ', file=out_file)
        print("D C")
        for c in cartesian_cylinders:
            print("%.1f %.1f" % c, end=' ', file=out_file)
            print(f"{c}")
        print(file=out_file)
    out_file.close()

