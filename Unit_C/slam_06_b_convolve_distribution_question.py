# Instead of moving a distribution, move (and modify) it using a convolution.
# 06_b_convolve_distribution
# Claus Brenner, 26 NOV 2012
from pylab import plot, show, ylim
from distribution import *
import matplotlib.pyplot as plt


def move(distribution, delta):
    """Returns a Distribution that has been moved (x-axis) by the amount of
       delta."""
    return Distribution(distribution.offset + delta, distribution.values)


def convolve(a, b):
    """Convolve distribution a and b and return the resulting new distribution."""

    # --->>> Put your code here.
    c_len = len(a.values) + len(b.values) - 1
    min_start = a.start()
    c = Distribution.create_distribution(min_start, c_len)
    a_values = a.values
    b_values = b.values
    c_values = c.values
    for i in range(len(a_values)):
        for j in range(len(b_values)):
            k = i + j
            if k < len(c_values):
                c_values[k] += a_values[i] * b_values[j]

    return move(c, b.offset)  # Replace this by your own result.


if __name__ == '__main__':
    arena = (0, 100)

    # Move 3 times by 20.
    moves = [5] * 10
    print(moves)

    # Start with a known position: probability 1.0 at position 10.
    position = Distribution.unit_pulse(10)
    plt.step(position.plotlists(*arena)[0], position.plotlists(*arena)[1])

    # Now move and plot.
    for m in moves:
        move_distribution = Distribution.triangle(m, 2)
        position = convolve(position, move_distribution)
        plt.step(position.plotlists(*arena)[0], position.plotlists(*arena)[1])

    plt.ylim(0.0, 1.1)
    plt.show()
