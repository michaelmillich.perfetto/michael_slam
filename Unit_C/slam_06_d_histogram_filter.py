# Histogram implementation of a bayes filter - combines
# convolution and multiplication of distributions, for the
# movement and measurement steps.
# 06_d_histogram_filter
# Claus Brenner, 28 NOV 2012
from pylab import plot, show, ylim
from distribution import *
import matplotlib.pyplot as plt


def move(distribution, delta):
    """Returns a Distribution that has been moved (x-axis) by the amount of
       delta."""
    return Distribution(distribution.offset + delta, distribution.values)


def multiply(a, b):
    """Multiply two distributions and return the resulting distribution."""

    # --->>> Put your code here.
    max_len = max(len(a.values), len(b.values))
    min_offset = min(a.offset, b.offset)
    c = Distribution.create_distribution(min_offset, max_len)
    for i in range(len(c.values)):
        k = i + c.offset
        temp = a.value(k) * b.value(k)
        c.set_value(index=k, value=temp)

    c.normalize()

    return c  # Modify this to return your result.


def convolve(a, b):
    """Convolve distribution a and b and return the resulting new distribution."""

    # --->>> Put your code here.
    c_len = len(a.values) + len(b.values) - 1
    min_start = a.start()
    c = Distribution.create_distribution(min_start, c_len)
    a_values = a.values
    b_values = b.values
    c_values = c.values
    for i in range(len(a_values)):
        for j in range(len(b_values)):
            k = i + j
            if k < len(c_values):
                c_values[k] += a_values[i] * b_values[j]

    return move(c, b.offset)  # Replace this by your own result.


# --->>> Copy your convolve(a, b) and multiply(a, b) functions here.


if __name__ == '__main__':
    arena = (0, 220)

    # Start position. Exactly known - a unit pulse.
    start_position = 10
    position = Distribution.unit_pulse(start_position)
    plt.step(position.plotlists(*arena)[0], position.plotlists(*arena)[1])

    # Movement data.
    controls = [20] * 10

    # Measurement data. Assume (for now) that the measurement data
    # is correct. - This code just builds a cumulative list of the controls,
    # plus the start position.
    p = start_position
    measurements = []
    for c in controls:
        p += c
        measurements.append(p)

    # This is the filter loop.
    for i in range(len(controls)):
        # Move, by convolution. Also termed "prediction".
        control = Distribution.triangle(controls[i], 10)
        position = convolve(position, control)
        plt.step(position.plotlists(*arena)[0], position.plotlists(*arena)[1], color='b')

        # Measure, by multiplication. Also termed "correction".
        measurement = Distribution.triangle(measurements[i], 10)
        position = multiply(position, measurement)
        plt.step(position.plotlists(*arena)[0], position.plotlists(*arena)[1],color='r')

    plt.show()
