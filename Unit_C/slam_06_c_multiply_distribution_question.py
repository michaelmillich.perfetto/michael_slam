# Multiply a distribution by another distribution.
# 06_c_multiply_distribution
# Claus Brenner, 26 NOV 2012
from pylab import plot, show
from distribution import *
import matplotlib.pyplot as plt


def multiply(a, b):
    """Multiply two distributions and return the resulting distribution."""

    # --->>> Put your code here.
    max_len = max(len(a.values),len(b.values))
    min_offset = min(a.offset,b.offset)
    c = Distribution.create_distribution(min_offset,max_len)
    for i in range(len(c.values)):
        k = i+c.offset
        temp = a.value(k) *b.value(k)
        c.set_value(index=k,value=temp)

    c.normalize()

    return c  # Modify this to return your result.


if __name__ == '__main__':
    arena = (0, 1000)

    # Here is our assumed position. Plotted in blue.
    position_value = 300
    position_error = 100
    position = Distribution.triangle(position_value, position_error)
    plt.step(position.plotlists(*arena)[0], position.plotlists(*arena)[1],
         color='b')

    # Here is our measurement. Plotted in green.
    # That is what we read from the instrument.
    measured_value = 550
    measurement_error = 200
    measurement = Distribution.triangle(measured_value, measurement_error)
    plt.step(measurement.plotlists(*arena)[0], measurement.plotlists(*arena)[1],
             color='g')

    # Now, we integrate our sensor measurement. Result is plotted in red.
    position_after_measurement = multiply(position, measurement)
    plt.step(position_after_measurement.plotlists(*arena)[0],
         position_after_measurement.plotlists(*arena)[1],
         color='r')

    plt.show()
